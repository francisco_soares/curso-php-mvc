<?php

// Namespace onde o compose consegue parametrizar e trazer esta classe em questão.
namespace App;

use MF\Init\Bootstrap;

class Route extends Bootstrap
{
    /**
     * Método que controla qual rota será enviada
     *
     * @return void
     */
    protected function initRoutes()
    {
        /**
         * Defino um [array] com [keys] e [value] especificos para:
         * [route = / ] - Define qual rota está sendo acessada
         * [controller = indexController] - Defino que o na pasta Controller, a classe que vai ser acessada é a indexController.php
         * [action = index] - Defino que o método da classe indexController que será acessado, nesse caso o método index 
         */
        $routes['home'] = array(
            'route' => '/',
            'controller' => 'indexController',
            'action' => 'index'
        );

        $routes['sobre_nos'] = array(
            'route' => '/sobre_nos',
            'controller' => 'indexController',
            'action' => 'sobreNos'
        );

        // Pego o método [setRoute] e passo por parametro os arrays definidos aqui no metodo [initRoutes]
        // Dessa forma é feito a inicialização do array de rotas.
        $this->setRoutes($routes);
    }
}
