<?php

namespace App;

use PDO;
use PDOException;

class Connection
{
    /**
     * Método responsavel por criar a conexão com o banco de dados
     *
     * COm o static, apenas passo a referencia da classe (Connection::). Assim não preciso instância-lá
     * 
     * [DICA IMPORTANTE] - se eu usar host=localhost; vai apresentar erro, sendo assim, o correto é usar host=127.0.0.1;
     * @return $conn
     */
    public static function getDb()
    {
        try {
            // Instancio o PDO para fazer a conexão
            $conn = new PDO(
                "mysql:host=127.0.0.1;dbname=estudo_mvc;charset=utf8",
                "root",
                ""
            );

            return $conn;
        } catch (PDOException $e) {
            // Aqui trato o erro
            echo "Erro: {$e->getMessage()} | {$e->getFile()} | Linha: {$e->getLine()}";
        }
    }
}
