<?php

namespace App\Controllers;

// Os Models
use App\Models\Info;
use App\Models\Produto;

// Os recursos do Miniframework
use MF\Controller\Action;
use MF\Model\Container;

class IndexController extends Action
{
    public function index()
    {
        // Aqui estou passando por parametro no getModel() o Produto, que está dentro de App\Model\
        $produto = Container::getModel('Produto');

        // Acesso o método getProdutos() class Produto
        $produtos = $produto->getProdutos();
        // Passo a referência do retorno para o $this->view->dados onde será manipulado no index.php da Views/index/index.php
        $this->view->dados = $produtos;

        // No método render(), além do parametro da VIEW, é passado um parametro com o tipo de layout solicitado.
        // Caso passe um layout que não exista, dentro do render() existe uma validação para isso.
        $this->render('index', 'layout1');
    }

    public function sobreNos()
    {

        // Aqui estou passando por parametro no getModel() o Produto, que está dentro de App\Model\
        $info = Container::getModel('Info');

        // Acesso o método getProdutos() class Produto
        $informacoes = $info->getInfo();

        // Passo a referência do retorno para o $this->view->dados onde será manipulado no index.php da Views/index/sobreNos.php
        $this->view->dados = $informacoes;

        // No método render(), além do parametro da VIEW, é passado um parametro com o tipo de layout solicitado.
        // Caso passe um layout que não exista, dentro do render() existe uma validação para isso.
        $this->render('sobreNos', 'layout2');
    }
}
