<?php

namespace App\Models;

use MF\Model\Model;

class Info extends Model
{
    /**
     * Método que faz o select dentro da tabela tb_produtos e retorna o statement, onde informo para trazer em array com o fetchAll()
     *
     * @return array
     */
    public function getInfo()
    {
        // Crio a query com as regras que quero.
        $query = "SELECT titulo, descricao FROM tb_info";

        // retorna um statement, assim posso tratar
        // Com o fetchAll() eu estou informando para trazer todos os resultados em array.
        return $this->db->query($query)->fetchAll();
    }
}
