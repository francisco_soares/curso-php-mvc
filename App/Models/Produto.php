<?php

namespace App\Models;

use MF\Model\Model;

class Produto extends Model
{
    /**
     * Método que faz o select dentro da tabela tb_produtos e retorna o statement, onde informo para trazer em array com o fetchAll()
     *
     * @return array
     */
    public function getProdutos()
    {
        // Crio a query com as regras que quero.
        $query = "SELECT id, descricao, preco FROM tb_produtos";

        // retorna um statement, assim posso tratar
        // Com o fetchAll() eu estou informando para trazer todos os resultados em array.
        return $this->db->query($query)->fetchAll();
    }
}
