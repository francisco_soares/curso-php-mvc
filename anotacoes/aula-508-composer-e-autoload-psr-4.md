COMPOSER E AUTOLOAD PSR-4

O Composer é um gerenciador de dependência para PHP. 
Onde conseguimos trazer pacotes externos para implementar nosso projeto.


[Autoload PSR-4]
Mapea todas as estruturas do projeto e trás as classes que contenha nos scripts através de [namespace]

Nessa aula está sendo configurado o Compose e autoload de arquivos com PSR-4.

[CONFIGURAÇÃO DO COMPOSER]

// [name] - Nome da aplicação/proejto que está criando
// [description] - Descrição sobre o projeto
// [authors] - Um array de objetos literais onde devo inserir as informações do autor
// [authors][{name}] - Nome do autor
// [authors][{email}] - Email do autor
// [authors][{role}] - Função ou cargo do autor [Developer]
// [minimum-stability] - Define a instabilidade do composer, o mesmo poser ser [stable]
// [require] - Tipos de recursos que são obrigatórios para o projeto
// [require]{php: >= 7.0} - Defino que a versão do PHP tem que ser igual ou maior que 7.0.
// [autoload] - Qual o método de autoload das classes do projeto
// [autoload][psr-4] - Define a forma que os arquivos são carregados
// [autoload][psr-4]{"App\\": "App/"} - Tipo de diretório que será mapeado com o namespace. IMPORTANTE sempre colocar o nome da pasta
// [vendor][MF] - é o diretório onde vai está inserido os pacotes da nossa aplicação.