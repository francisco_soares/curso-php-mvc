# **UM POUCO MAIS SOBRE MVC**

## **CLIENTE**
- Protocolo HTTP (Request / Response)

## **CONTROLLER**
- Recebe todas as requisições e controla o que deve acontecer e quando

## **MODEL** -> [Banco de dados]
- Camada de banco de dados e regras de negócio

## **VIEW**
- Exição dos dados (html, xml, json)

![PRINT - UM POUCO MAIS SOBRE MVC](img/aula-507-1.png)