# **INTRODUÇÃO AO PADRÃO DE ARQUITETURA MVC**

## **Padrão de arquitetura (Architectural Pattern)**
Padrão de Arquitetura é a forma como as funções são organizadas no Software<br><br>

- Cliente-Servidor
- P2P
- Quadro Negro
- Criação implícita
- Pipes e filtros
- Plugin
- Analise de sistema estruturado
- Arquitetura orientada a serviço
- Arquitetura orientada a busca
- **MVC**
- Computação distribuída
- Aplicação monolítica
- Modelo em três camadas

<br><br>

## **Padrão de projeto (Design Pattern)**<br>
Padrão de Projeto: Como certas funcionalidades são desenvolvidas no software. Técnicas de implementação de funcionalidades a nivel de código que foram testado no passado e são usadas como boas práticas.

**Padrões de criação**
- Abstract Factory
- Builder
- Factory Method
- Prototype
- **Singleton**
<br>

**Padrões estruturais**
- Adapter
- Facade
- Bridge
- Decorator
- FlyWight
- Composite
- Proxy
<br>

**Padrões comportamentais**
- Chain of Responsability
- Command
- Interpreter
- Iterator
- Mediator
- Memento
- Observer


------

![Print - Introdução ao padrão de arquitetura MVC](img/aula-506-1.png)

------

## **REQUISITOS FUNCIONAIS**
- Recursos da aplicação em si, ou seja, ligados diretamente as funcionalidades da aplicação
Ex: Autenticação de um usuário via formulário, Carrinho de compras e etc

- Design Pattern: É possivel identificar vários Design Pattern ou padrões de projetos e são usadas para executar as funcionalidades da aplicação
<br><br>

## **REQUISITOS NÃO FUNCIONAIS**
- Recursos que não estão diretamente relacionados com as funcionalidades da aplicação.
Ex: O tempo de processamento, disponibilidade, compatibilidade, segurança, portabilidade e usabilidade e etc.

- Architectural Pattern: Conseguimos definir qual melhor Architectural Pattern para estruturar o projeto como um todo.

![Print - Introdução ao padrão de arquitetura MVC](img/aula-506-2.png)
![Print - Introdução ao padrão de arquitetura MVC](img/aula-506-3.png)