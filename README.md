# **PHP MVC**

_Entendendo como o MVC funciona._
<br><br>

## **Antes de começar**
------------------------------<br>
Abra o Windown terminal e acesse a pasta **Public** do **MiniFrameWork**<br>
Código:<br>
> cd **C:\xampp\htdocs\study\2021\curso-php-mvc\public**
<br><br>

Após ter acessado a pasta você deve **iniciar o servidor interno do PHP**<br>
Código:<br>
> **php -S localhost:8000**
