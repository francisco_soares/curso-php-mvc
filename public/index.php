<?php
// Mostra os erros.
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

// Acesso o arquivo autoload.php, onde fará os carregamentos das classes definidas no arquivo composer.json
use App\Route;

require_once "../vendor/autoload.php";

// Estancio a classe Route
$route = new Route;