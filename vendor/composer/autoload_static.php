<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit83a7f160832627a17387af4ffdb10dc1
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'MF\\' => 3,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'MF\\' => 
        array (
            0 => __DIR__ . '/..' . '/MF',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/App',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit83a7f160832627a17387af4ffdb10dc1::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit83a7f160832627a17387af4ffdb10dc1::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit83a7f160832627a17387af4ffdb10dc1::$classMap;

        }, null, ClassLoader::class);
    }
}
