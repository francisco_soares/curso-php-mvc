<?php

namespace MF\Model;

use PDO;

abstract class Model
{
    // Atributo que vai receber a instancia do PDO(getDb)
    protected $db;

    /**
     * Método construtor que recebe por parametro a instancia de PDO
     *
     * @param PDO $db
     */
    public function __construct(PDO $db)
    {
        $this->db = $db;
    }
}
