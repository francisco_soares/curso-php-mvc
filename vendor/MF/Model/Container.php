<?php

namespace MF\Model;

use App\Connection;

class Container extends Connection
{

    public static function getModel($model)
    {

        // Crio a variável com o caminho para a instância, então ficará assim: \App\Model\Info; ou \App\Model\Produto;
        // Depende do que está vindo no parametro $model.
        $class =  "\\App\\Models\\" . ucfirst($model);

        // Instância da conexão, como o método getDb foi definido como static, eu consigo acessar dessa forma Connection::getDb()
        $conn = Connection::getDb();

        // Retornar o modelo solicitado já instânciado, inclusive com a conexão estabelecida
        return new $class($conn);
    }
}
