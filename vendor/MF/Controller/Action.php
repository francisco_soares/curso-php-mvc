<?php

namespace MF\Controller;

abstract class Action
{
    // Atributo protegido
    protected $view;

    /**
     * Método construtor da classe
     */
    public function __construct()
    {
        // o stdClass() é uma classe nativa do PHP que permite criar objetos padrões
        $this->view = new \stdClass();
    }

    /**
     * render() - Responsavel por carregar a view que está sendo carrada pelo usuário
     *
     * @param string $view
     * @return void
     */
    protected function render($view, $layout)
    {
        // A váriavel abaixo pega o parametro $view que vem lá do IndexController.php
        // O page é um atributo do stdClass() criado dinâmicamente.
        // Sendo assim, consigo usar o $this->view->page em qualquer lugar da classe.
        // Veja que o mesmo está sendo usado no método content().
        $this->view->page = $view;

        // Na condição abaixo, eu verifico se o arquivo do layout existe dentro da pasta App/Views. Caso exista, trás o layout.
        // Caso não exista, só e mostrado o conteúdo vindo do método $this->contetn()
        if (file_exists("../App/Views/$layout.php")) {
            require_once "../App/Views/$layout.php";
        } else {
            $this->content();
        }
    }

    /**
     * Método que retorna o conteúdo para o layout da página.
     *
     * @return void
     */
    protected function content()
    {
        // get_class() - Retorna o caminho onde esse arquivo IndexController.php está.
        $classAtual =  get_class($this);
        $classAtual = str_replace('App\\Controllers\\', '', $classAtual);
        $classAtual = strtolower(str_replace('Controller', '', $classAtual));

        // Aqui no require, o processo de acesso ao index.php começa dentro da pasta public, por isso temos que subir um nivel ../
        require_once "../App/Views/$classAtual/" . $this->view->page . ".php";
    }
}
