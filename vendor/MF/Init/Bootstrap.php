<?php

namespace MF\Init;

/**
 * Classe abstrada Bootstrap()
 *  * Uma classe abstrada é igual a qualquer outra classe, a diferença é que a mesma não pode ser INSTÂNCIADA.
 */
abstract class Bootstrap
{
    // Atributos privados da classe Bootstrap
    private $routes;

    /**
     * Método abstrato.
     * 
     * Quando existe uma método abstrado em uma classe abstrada, a classe filha que herda, deve implementar o método
     *
     * @return void
     */
    abstract protected function initRoutes();

    /**
     * Metodo contrutor da classe Route. Será executado assim que instânciar a classe Route.
     */
    public function __construct()
    {
        $this->initRoutes();
        $this->run($this->getUrl());
    }

    /**
     * Método que pega o valor que está no atributo $route
     *
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Metodo que setá o array de rotas.
     *
     * @param array $routes
     * @return void
     */
    public function setRoutes(array $routes)
    {
        $this->routes = $routes;
    }

    /**
     * Metodo run() - Recebe um parametro, isso seria a URL da pagina que está sendo acessado na aplicação
     *
     * @param string $url
     * @return void
     */
    protected function run($url)
    {
        // Faço um foreach para percorrer os arrays que vem do $this->getRoutes().
        foreach ($this->getRoutes() as $key => $route) {

            // Faço uma condição para verificar se $url é igual a $route['route']
            // Isso ficaria assim: $url = / && $route['route'] = /, se atender a essa condição entra no if.
            if ($url == $route['route']) {

                // Crio a referencia para instância da classe, onde o $route['controller'] retorna o IndexController como valor no array.
                // $class = "App\\Controllers\\IndexController"
                $class = "App\\Controllers\\" . ucfirst($route['controller']);

                // Crio a instância da classe Controller
                $controller = new $class;

                // Seto o valor da action que vem do array na variável $action
                $action = $route['action'];

                // Acesso o método que é passado na $action
                // fica assim: $controller->sobreNos()
                $controller->$action();
            }
        }
    }
    /**
     * Método que trás a URL que está sendo acessada no momento. 
     *
     * $_SERVER['REQUEST_URI'] - Retorna a raiz da URL, se você estiver na home, será retornado [/], caso você esteja na página contato, será retornado [contato]
     * 
     * parse_url - Recebe a URL, interpreta essa URL e retorna seus respectivos componentes. Ela retorna um array com os componentes especificos. O componente retornado no Array é o [path]
     * 
     * PHP_URL_PATH - Quando submetida no [parse_url], trás apenas o retorno da string da URL. Dessa forma, a key [path] é desconsiderada
     * 
     * @return array
     */
    protected function getUrl()
    {
        return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    }
}
